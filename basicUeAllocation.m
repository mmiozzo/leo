%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%


function [rbsActiveBs, usedRbPerBs,ueTrafficAllocated, activeBsId, sleepBsId, ueAllocation] = basicUeAllocation(ueTrafficPerSlotArray, slotTrafficCoeff, ueAllocation, ueDlSinrMatrix, ueDlSinrArray, rbsAllocatedPerBsRatio, bsBatteryLevel, bsSwitchOffThr)
global debuglevel;
global N_UE;
global N_BS;
global N_RB;
global timeSlot;


% udpate bs status
% reactivate all BSs
rbsActiveBs = ones(1,N_BS) * N_RB;
% Switch off BSs under threshold
bsOutOfBatteriesIds = find ((bsBatteryLevel.-bsSwitchOffThr)<0);
rbsActiveBs(bsOutOfBatteriesIds) = 0;
activeBsId = find(rbsActiveBs>0);
%  debug ('Sleep BSs:');
sleepBsId = find(rbsActiveBs==0);


% first re-allocate UE disconnected
for ue=1:N_UE
    if (ueAllocation(ue)==0)
      ueAllocationTemp = max (ueDlSinrMatrix');
      ueDlSinrArray = ueAllocation;
      ueAllocation(i) = find (ueDlSinrMatrix(i,:)==ueAllocation(i));
    end
endfor


slotTrafficArray = ueTrafficPerSlotArray * slotTrafficCoeff;
ueTbSizeArray = get_tb_size (ueDlSinrArray);
ueRbReq = (slotTrafficArray*1024*1024)./ueTbSizeArray;
%debug('Ue RBs Req'); ueRbReq
availableRbPerBs = rbsActiveBs*1000*60*60*timeSlot;
usedRbPerBs = zeros(1,N_BS);
ueTrafficAllocated = zeros(1,N_UE);

for ue=1:N_UE
   if (ueRbReq>0)
      if (availableRbPerBs(ueAllocation(ue))>=ueRbReq(ue)+usedRbPerBs(ueAllocation(ue)))
          % UE fit in assignted BS
          usedRbPerBs(ueAllocation(ue)) += ueRbReq(ue);
          ueTrafficAllocated(ue) = slotTrafficArray(ue);
          %printf('UE %d(%f) allocated to BS %d(%f)\n', ue, ueRbReq(ue), ueAllocation(ue), usedRbPerBs(ueAllocation(ue)));
      else
          % check for another BS
          dlSinrArray = ueDlSinrMatrix(ue,:);
          bsNum = 1;
          dlSinrArray(ueAllocation(ue)) = -Inf;
          while (bsNum<N_BS)
            max_sinr = max(dlSinrArray);
            ueAllocation(ue) = find (dlSinrArray==max_sinr);
            tbSize = get_tb_size (max_sinr);
            if (tbSize>0)
              ueRbReq(ue) = (slotTrafficArray(ue)*1024*1024)/tbSize;
              if (availableRbPerBs(ueAllocation(ue))>=ueRbReq(ue)+usedRbPerBs(ueAllocation(ue)))
                usedRbPerBs(ueAllocation(ue)) += ueRbReq(ue);
                ueTrafficAllocated(ue) = slotTrafficArray(ue);
                ueDlSinrArray(ue) = max_sinr;
                break;
              else
                dlSinrArray(ueAllocation(ue)) = -Inf;
              end
            else
              % UE out fo coverage of any BS
              ueAllocation(ue) = 0; % user dropped
              break;
            endif
            bsNum = bsNum + 1;
          endwhile
          if ((ueAllocation(ue)~=0)&&(dlSinrArray(ueAllocation(ue)) == -Inf))
              ueAllocation(ue) = 0; % user dropped
          endif
      endif
   else
     % UE out fo coverage of any BS
     ueAllocation(ue) = 0; % user dropped
  endif
endfor
%debug ('UE Allocation'); ueAllocation
endfunction