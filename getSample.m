%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

function s = getSample(x,cdf)

  %%
  %% function 2 = getSample(x,cdf)
  %%
  %% returns the a sample from the cdf distribution
  %%

  rp = rand(1);
  if rp == 0
    s = x(1);
  elseif rp == 1
    s = x(end);
  else
    fp = find(cdf<=rp,1,'last');
    s = x(fp)+(x(fp+1)-x(fp))*(rp-cdf(fp))/(cdf(fp+1)-cdf(fp));
  end

endfunction
     