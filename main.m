%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%


function [] = main (simSeed, scNum, scVoltage, mcNum, scBatteryCapacity, ueNum, uePerSc, rbNum, algorithm, debugLevel, algo_params, cdfMatrixName, panelName, energyClustersNum, nDays, algo_string, month, batteryThreshold)


global debuglevel;
debuglevel = debugLevel;


% when =1 the SolarStat statistics are used only for generating 1 day and 
% then repeated for the whole simulation
% otherwise re-generated each day
dailyFixedSolarStat = 0;

%  function [] = debug(s)
%  if (debuglevel~=0)
%    printf('%s\n',s);
%  end
%  endfunction

rand ('seed', simSeed);

global N_RB;
N_RB = rbNum;

% power drained by a small cell
if (scVoltage==12)
    % Pico cell [D2.3 EARTH]
    P_sc_0 = 13.6; % [W]
    P_sc_max = 14.7; % [W]
  else
    % Micro cell [D2.3 EARTH]
    P_sc_0 = 105.6; % [W]
    P_sc_max = 144.6; % [W]
endif
P_sc_vect = P_sc_0:(P_sc_max-P_sc_0)/N_RB:P_sc_max;

% power drained by a macro cell
P_mc_0 = 750.0; %[W]
P_mc_max = 1350; %[W]
P_mc_vect = P_mc_0:(P_mc_max-P_mc_0)/N_RB:P_mc_max;

N_SC = scNum;
% if scGridXnum>0 and  scGridYnum>0 N_SC is the number of SCs in the grid
scGridXnum = 0;
scGridYnum = 0;
if ((scGridXnum>0)&&(scGridYnum>0))
  N_SC = scGridXnum*scGridYnum;
end
global N_MC;
N_MC = mcNum;
global N_BS;
N_BS = N_MC + N_SC;
mcIds = 1:N_MC;

% RBs array
%rbsActiveBs = ones(1,N_BS) * N_RB;

global N_UE;
N_UE = ueNum;

if ((month>=1)&&(month<=12))
  suffix = sprintf('%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d_month%d_seed%d.txt', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE, month, simSeed);
  suffixSim = sprintf('%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d_month%d.txt', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE, month);
  suffixFigure = sprintf('%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d_month%d', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE, month);
else
  suffix = sprintf('%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d_seed%d.txt', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE, simSeed);
  suffixSim = sprintf('%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d.txt', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE);
  suffixFigure = sprintf('%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE);
end

% simulation area
x_field = 1000; %[m]
y_field = 1000; %[m]

bsPosX = zeros(1,N_BS);
bsPosY = zeros(1,N_BS);

% place MCs
% grid TBD -> single cell -> place in the middle
if (N_MC>1)
  error ('Sim draft version: only single macro cell scenario');
end
bsPosX(1) = x_field / 2;
bsPosY(1) = y_field / 2;

if (scVoltage==12)
  scCoverage = 30; % pico
else
  scCoverage = 50; % micro
end

% place SCs
if (scGridXnum==0)
  for i=N_MC+1:N_BS
    % place SCs and avoid overlap in coverage
    do
      bsPosX(i) = rand (1)*x_field;
      bsPosY(i) = rand (1)*y_field;
      overlap = 0;
      for (j=N_MC+1:i-1)
        printf('SC %d (%g,%g) SC %d (%g,%g), distance %g \n', i, bsPosX(i), bsPosY(i), j, bsPosX(j), bsPosY(j), sqrt((bsPosX(i)-bsPosX(j))^2+(bsPosY(i)-bsPosY(j))^2));
        distance = sqrt((bsPosX(i)-bsPosX(j))^2+(bsPosY(i)-bsPosY(j))^2);
        if (distance<scCoverage)
          overlap = 1;
  %          printf('DISCARED\n');
          break;
        end
      end
      mc_distance = sqrt((bsPosX(i)-bsPosX(1))^2+(bsPosY(i)-bsPosY(1))^2)
      if (mc_distance<2*scCoverage) % take some more margin due to high Tx power of macro
        overlap = 1;
  %        printf('DISCARED\n');
      end
    until (overlap==0);
  end
else
  % place a grid of scGridXnum x scGridYnum with adjacent nodes distance in grid: gridUnit
  gridUnit = 333.3;
  gridX = gridUnit*(scGridXnum-1);
  gridY = gridUnit*(scGridYnum-1);
  yPos = (y_field/2) - (gridY/2);
  scId = N_MC+1;
  for yId=1:scGridYnum
    xPos = (x_field/2) - (gridX/2);
    for xId=1:scGridXnum
      bsPosX(scId) = xPos;
      bsPosY(scId) = yPos;
      xPos = xPos + gridUnit;
      scId = scId + 1;
    end
    yPos = yPos + gridUnit;
  end
end

bsPosX
bsPosY


% Place UEs
if (uePerSc==0)
  % uniform distribution in the whole area
  uePosX = rand (1, N_UE) * x_field
  uePosY = rand (1, N_UE) * y_field
else
  % uniform distribution of UEs in each SC
  uePosX = zeros(1,N_UE);
  uePosY = zeros(1,N_UE);
  scUeAllocated = 0;
  scId = N_MC+1;
  for i=1:N_UE
    do 
      uePosX(i) = ((rand (1)*2)-1)*scCoverage + bsPosX(scId);
      uePosY(i) = ((rand (1)*2)-1)*scCoverage + bsPosY(scId);
      distance = sqrt((bsPosX(scId)-uePosX(i))^2+(bsPosY(scId)-uePosY(i))^2);
    until ((uePosX(i)>=0)&(uePosX(i)<=x_field)&(uePosY(i)>=0)&(uePosY(i)<=y_field)&(distance<=scCoverage));
    scUeAllocated = scUeAllocated + 1;
    printf('SC %d (%g,%g) UE %d (%g,%g), distance %g \n', scId, bsPosX(scId), bsPosY(scId), i, uePosX(i), uePosY(i), sqrt((bsPosX(scId)-uePosX(i))^2+(bsPosY(scId)-uePosY(i))^2));
    if (scUeAllocated==uePerSc)
      if (scId>N_MC)
        scId = scId + 1;
        if (scId>N_BS)
          scId = 1; % uniformely distributed in the whole area
          if (x_field<y_field)
            scCoverage = x_field/2;
          else
            scCoverage = y_field/2;
          end
        end
        scUeAllocated = 0;
      end
    end
  end %for N_UE
end
    
  


% batteries
scBattery = 1; % when 1 means SCs are powered only by batteries, while 0 means only by grid (battery and solar panel will be ignored)
mcBattery = 0; % when 1 means MCs are powered only by batteries, while 0 means only by grid
bsBatteryArray = ones(1,N_BS) * scBattery;
bsBatteryArray(mcIds) = mcBattery;
if (scBattery==0)
  bsGridArray = ones(1,N_BS);
else
  bsGridArray = zeros(1,N_BS);
endif
if (mcBattery==0)
  bsGridArray(mcIds) = 1;
else
  bsGridArray(mcIds) = 0;
endif

%scBatteryCapacity = 1; %[KWh]
mcBatteryCapacity = 1; %[KWh]
bsBatteryCapacityArray = ones(1,N_BS) * scBatteryCapacity;
bsBatteryCapacityArray(mcIds) = mcBatteryCapacity;
scInitialBatteryCharge = scBatteryCapacity/2; %[KWh]
%mcInitialBatteryCharge = 0; %[KWh]     % Macro switched off
mcInitialBatteryCharge = 1; %[KWh]    % macro switched on
bsBatteryChargeArray = ones(1,N_BS) * scInitialBatteryCharge;
bsBatteryChargeArray(mcIds) = mcInitialBatteryCharge;
bsBatteryLevel = bsBatteryChargeArray./bsBatteryCapacityArray
%scVoltage = 24; % V
%mcVoltage = 42; % V
mcVoltage = 0; % V  macro BS without solar panel
bsVoltageArray = ones(1,N_BS) * scVoltage;
bsVoltageArray(mcIds) = mcVoltage;
%scSwitchOnThr = 0.1;
%mcSwitchOnThr = 0.1;
%bsSwitchOnThr = ones(1,N_BS) * scSwitchOnThr;
%bsSwitchOnThr(mcIds) = mcSwitchOnThr;

scSwitchOffThr = batteryThreshold;
mcSwitchOffThr = batteryThreshold;
bsSwitchOffThr = ones(1,N_BS) * scSwitchOffThr;
bsSwitchOffThr(mcIds) = mcSwitchOffThr;

scSecurityThr = algo_params{7};
mcSecurityThr = algo_params{7};
bsSecurityThr = ones(1,N_BS) * scSecurityThr;
bsSecurityThr(mcIds) = mcSecurityThr;

% BS energy consumption

if (scVoltage==12)
    % Pico cell [D2.3 EARTH]
    scW0 = 0.0116; % [kW]
    sc_a = 0.0011;
  else
    % Micro cell [D2.3 EARTH]
    scW0 = 0.1056; % [kW]
    sc_a = 0.039;
endif
mcW0 = 0.750; % [kW]
mc_a = 0.600;
%mcW0 = 0.0; % [kW]
%mc_a = 0.0;
bsW0Array = ones(1,N_BS) * scW0;
bsW0Array(mcIds) = mcW0;
bs_aArray = ones(1,N_BS) * sc_a;
bs_aArray(mcIds) = mc_a;

% traffic ditribution (see D2.3 of earth project)
heavy_users_ratio = 0.2;
heavy_traffic = 900; %[MB/h]
ordinary_traffic = 112.5; %[MB/h]
%traffic_profile = [0.13 0.10 0.07 0.04 0.03 0.02 0.02 0.03 0.04 0.06 0.08 0.09 0.10 0.10 0.11 0.12 0.12 0.12 0.13 0.14 0.15 0.16 0.16 0.15]; % earth (residential)


traffic_profile = [0.01 0.01 0.01 0.04 0.08 0.11 0.13 0.15 0.16 0.16 0.16 0.16                    0.11 0.11 0.16 0.15 0.14 0.13 0.10 0.08 0.05 0.03 0.02 0.01];% urban

ueTrafficArray = ones(1, N_UE) * ordinary_traffic;
if (uePerSc>0)
  heavy_users_perSc_Num = heavy_users_ratio*N_UE/N_SC;
  if (heavy_users_perSc_Num>uePerSc)
     error('Too much heavy users per SC');
  end
  scUeAllocated = 0;
  scHeavyUeAllocated = 0;
  for i=1:N_UE
    if (scHeavyUeAllocated<heavy_users_perSc_Num)
      ueTrafficArray(i) = heavy_traffic;
      scHeavyUeAllocated = scHeavyUeAllocated + 1;
    end
    %printf('UE %d traffic %g\n', i, ueTrafficArray(i));
    scUeAllocated = scUeAllocated + 1;
    if (scUeAllocated==uePerSc)
      scUeAllocated = 0;
      scHeavyUeAllocated = 0;
    end
  end % for
else
  heavy_users_id = 1:heavy_users_ratio*N_UE;
  ueTrafficArray (heavy_users_id) = heavy_traffic;
end
ueTrafficArray

% power initialization
warning ('Preliminar UEs allocation version  with single macro cell');
if (scVoltage==12)
  scPower = 21;  % [dBm] PICO
else
  scPower = 38;  % [dBm] MICRO
end
mcPower = 43;  % [dBm]
%mcPower = 0;  % [dBm]
uePower = 9;  % [dBm]
bsPowerArray = ones(1, N_BS) * scPower;
bsPowerArray (mcIds) = mcPower;
bw = N_RB * 180000; % bandwidth in Hz, note that this is smaller than
        % the nominal Channel Bandwdith, see TS 36.101 fig 5.6-1
kT = -174; % noise PSD in dBm / Hz
noiseFigure = 9; % receiver noise figure in dB
noisePower = kT + 10*log10(bw) + noiseFigure  % noise power dBm
scHeigth = 5; % [m]
mcHeigth = 40; % [m]
bsHeightArray = ones (1, N_BS) * scHeigth;
bsHeightArray (mcIds) = mcHeigth;
ueHeigth = 2; % [m]
ueHeightArray = ones (1, N_UE) * ueHeigth;
distances = ueDistances(uePosX, uePosY, bsPosX, bsPosY)
frequency = 2160;  % carrier freq MHz, EARFCN = 500 (downlink)
ueDlLossMatrix = loss_OH_large_cities_urban(distances, bsHeightArray, ueHeightArray, frequency)
% debug code for verifying the new version of loss_OH_large_cities_urban
%  g = [];
%  for i=1:N_UE
%    gi = [];
%    for j=1:N_BS
%       i
%       j
%       distances(i,j)
%        bsHeightArray(j)
%       ueHeightArray (i)
%       gij = loss_OH_large_cities_urban_old(distances(i,j), bsHeightArray(j), ueHeightArray (i), frequency);
%       gi = cat (2, gi, gij);
%    end
%    g = cat (1, g, gi);
%  end
%  g
%  
%  ueLossArray - g

%ueDlSinrMatrix = evaluateSinr (bsPowerArray, ueDlLossMatrix, noisePower)
ueDlSinrMatrix = evaluateSnr (bsPowerArray, ueDlLossMatrix, noisePower)
% initial allocation (best SINR)
ueAllocation = max (ueDlSinrMatrix');
ueDlSinrArray = ueAllocation;
for i=1:N_UE
  ueAllocation(i) = find (ueDlSinrMatrix(i,:)==ueAllocation(i));
endfor
printf('Initial allocation map\n');
ueAllocation
% simulation parameters
N_DAYS = nDays;
initialMonth = 1;
global timeSlot;
timeSlot = 1; % [h] (in case is less than 1 hour use "decimal" [0..1] format)
slotsPerDay = 24 / timeSlot;
if (mod(24,timeSlot)~=0)
  error('Error timeSlot must be an integer that divides 24');
end
if (timeSlot>1)
  % adjust traffic profile vector
  traffic_profile
  slotTrafficProfile = [];
  %tid = 1:timeSlot
  tid = timeSlot
  traffic_profile(tid)
sum(traffic_profile(tid))
  for i=1:slotsPerDay
    x = sum(traffic_profile(tid))/timeSlot;
    slotTrafficProfile = cat(2, slotTrafficProfile, x);
    tid = tid + timeSlot;
  end
  slotTrafficProfile
elseif (timeSlot==1)
   slotTrafficProfile = traffic_profile;
elseif (timeSlot<1)
   slotTrafficProfile = [];
   slotPerHour = 1/timeSlot;
   for i=1:slotsPerDay
     ceil(i/slotPerHour)
     slotTrafficProfile = cat(2, slotTrafficProfile, traffic_profile(ceil(i/slotPerHour)));
   end
   traffic_profile
   slotTrafficProfile
end
% adjust traffic req
ueTrafficPerSlotArray = ueTrafficArray * timeSlot;

% read solar energy distribution
% read cdf of current distribution and time distribution generated by SolarStat

load (cdfMatrixName);
printf('Load cdf matrixes: %s\n', cdfMatrixName);

%  xc_array = xc_matrix{:,initialMonth};
%  cdfc_array = cdfc_matrix{:,initialMonth};
%  xtau_day = xtau_day_matrix{:,initialMonth};
%  cdftau_day = cdftau_day_matrix{:,initialMonth};
%  plot(xc_array{:,2},cdfc_array{:,2},'-b','LineWidth',1,...
%                    'MarkerEdgeColor','b',...
%                    'MarkerFaceColor','b',...
%                    'MarkerSize',3);
%  plot(xtau_day,cdftau_day,'-b','LineWidth',1,...
%                    'MarkerEdgeColor','b',...
%                    'MarkerFaceColor','b',...
%                    'MarkerSize',3);


N_ENERGY_SLOT = energyClustersNum;
slotDurationArray = ones(1, N_ENERGY_SLOT) * 24/N_ENERGY_SLOT;
slotCurrentArray = zeros(1, N_ENERGY_SLOT);
if (N_ENERGY_SLOT~=numel(xc_matrix{:,1}))
  error('SolarStat tables uncompatible, loaded %d  while needed %d', numel(xc_matrix{:,1}), N_ENERGY_SLOT);
end
energySlot = 0; % night
xc_array = xc_matrix{:,initialMonth};
cdfc_array = cdfc_matrix{:,initialMonth};
if (dailyFixedSolarStat==1)
  for i=1:N_ENERGY_SLOT
    slotCurrentArray(i) = getSample(xc_array{:,i}, cdfc_array{:,i});
  end
  currentSlot = slotCurrentArray(1);
else
  currentSlot = getSample(xc_array{:,energySlot+1}, cdfc_array{:,energySlot+1})
end
if (N_ENERGY_SLOT==2)
  xtau_day = xtau_day_matrix{:,initialMonth};
  cdftau_day = cdftau_day_matrix{:,initialMonth};
  dayDuration = getSample(xtau_day,cdftau_day);
  printf('Duration of the day %f hours\n', dayDuration);
  next_slot_change_time = (24-dayDuration)/2
else
  next_slot_change_time = (24/N_ENERGY_SLOT)+1;
end

rbsAllocatedPerBs = zeros (1,N_BS);
rbsAllocatedPerBsRatio = zeros (1,N_BS);
trafficReqPerBs = zeros (1,N_BS);
activeBsId = 1:N_BS;
sleepBsId = [];

% Statistics files
batteryLevelFilename = sprintf('scBatteryLevel_%s', suffix);
energyDrainedFilename = sprintf('energyDrained_%s', suffix);
rbsActiveBsFilename = sprintf('rbsActiveBs_%s', suffix); % active RBs per TTI 
usedRbPerBsFilename = sprintf('usedRbPerBs_%s', suffix); % used RBS per TTI
ueTrafficAllocatedFilename = sprintf('ueTrafficAllocated_%s', suffix);
hourAggregatedStatsFilename = sprintf('hourAggregatedStats_%s', suffix);
dayAggregatedStatsFilename = sprintf('dayAggregatedStats_%s', suffix);
fpBatteryLevel = fopen(batteryLevelFilename,'w');
fpEnergyDrained = fopen(energyDrainedFilename, 'w');
fpRbsActiveBs = fopen(rbsActiveBsFilename,'w');
fpUsedRbPerBs = fopen(usedRbPerBsFilename,'w');
fpUeTrafficAllocated = fopen(ueTrafficAllocatedFilename,'w');
fpHourAggregatedStats = fopen(hourAggregatedStatsFilename,'w');
fpDayAggregatedStats = fopen(dayAggregatedStatsFilename,'w');


statsSlotNum = 2;
winterMonths = [1 2 10 11 12];
simDays = zeros(1,statsSlotNum);
simAggTrafficReq = zeros(1,statsSlotNum);
simAggThr = zeros(1,statsSlotNum);
simAggThrOffloaded = zeros(1,statsSlotNum);
simAvgThr = zeros(1,statsSlotNum);
simScsSleeping = zeros(1,statsSlotNum);
simDroppedUes = zeros(1,statsSlotNum);
simAggEnergyWasted = zeros(1,statsSlotNum);
simAverageBatteryLevel = zeros(1,statsSlotNum);
simGridEnergyWasted = zeros(1,statsSlotNum);
simAverageMcsLoad = zeros(1,statsSlotNum);
simDailyPeakMcsLoad = zeros(1,statsSlotNum);
simHoursBatteryBelowThr = zeros(1,statsSlotNum);
simAverageMinBatteryLevel = zeros(1,statsSlotNum);
simAverageMaxBatteryLevel = zeros(1,statsSlotNum);
simAverageNormalizedThroughput = zeros(1,statsSlotNum);
simConvergenceTime = ones(1,statsSlotNum)*-1;
simConvergenceNum = zeros(1,statsSlotNum);
simFairness = zeros(1,statsSlotNum);
simThrDrop = zeros(1,statsSlotNum);


simAvgThr_2 = zeros(1,statsSlotNum);
simAverageBatteryLevel_2 = zeros(1,statsSlotNum);
simAverageMcsLoad_2 = zeros(1,statsSlotNum);
simAverageMinBatteryLevel_2 = zeros(1,statsSlotNum);
simAverageMaxBatteryLevel_2 = zeros(1,statsSlotNum);
simAverageNormalizedThroughput_2 = zeros(1,statsSlotNum);



convergenceWindow = 3;
convergenceSeries = 0;
convergence = 0;
if (algorithm==0) % algo 0 is greedy, no convergence
  convergence = 1;
endif
convergenceDay = 0;
convergenceThr = 0.05;

% policy output
sampleDayPolicy = [];


totalRbPerBs = ones(N_BS)*N_RB*1000*60*60*timeSlot;

totThroughputNormalized = 1;

for day=1:N_DAYS
  if ((month>=1)&&(month<=12))
    monthId = month;
  else
    dayno = mod(day, 365);
    monthId = month_id(dayno, initialMonth);
  end
  printf('\n-------------------------------------------------------\n');
  printf('-------------------- DAY %2d MONTH %2d ------------------\n', day, monthId);
  printf('-------------------------------------------------------\n\n');
  dailyAggTrafficReq = 0;
  dailyAggThr = 0;
  dailyAggThrOffloaded = 0;
  dailyScsSleeping = 0;
  dailyDroppedUes = 0;
  dailyAggEnergyWasted = 0;
  dailyAverageBatteryLevel = 0;
  dailyGridEnergyWasted = 0;
  dailyAverageMcsLoad = 0;
  dailyPeakMcsLoad = 0;
  dailyHoursBatteryBelowThr = 0;
  dailyMinBatteryLevel = 1;
  dailyMinBatteryLevelArray = ones(1,N_BS);
  dailyMaxBatteryLevel = 0;
  dailyAverageNormalizedThroughput = 0;
  dailyAvgFairness = 0;
  dailyThrDrop = 0;
  for slot=1:slotsPerDay
    it = slot + ((day-1)*slotsPerDay);
    printf('-------->Processing SLOT %d of day %d, month %d (it %d)\n', slot, day, monthId,it);
    % update batteries recharge
    if ((slot+1)*timeSlot>next_slot_change_time)
      previousSlotTime = next_slot_change_time - (slot*timeSlot);
      newSlotTime = timeSlot - previousSlotTime
    else
      previousSlotTime = timeSlot;
    end
    previousSlotTime
    harvestedEnergy = bsVoltageArray * currentSlot * previousSlotTime /1024; % [kWh]
    if ((slot+1)*timeSlot>next_slot_change_time)
      % update energy process
      energySlot = mod(energySlot+1, N_ENERGY_SLOT);
      xc_array = xc_matrix{:,monthId};
      cdfc_array = cdfc_matrix{:,monthId};
      if (dailyFixedSolarStat==1)
        currentSlot = slotCurrentArray(energySlot+1);
      else
        currentSlot = getSample(xc_array{:,energySlot+1}, cdfc_array{:,energySlot+1});
      end
      if (N_ENERGY_SLOT==2)
        if (energySlot==1)
          next_slot_change_time = mod(next_slot_change_time+dayDuration,24);
        else
          if (dailyFixedSolarStat~=1)
            dayDuration = getSample(xtau_day,cdftau_day);
          end
          next_slot_change_time = 24 + (24-dayDuration)/2
          %currentSlot=0;
        end
        harvestedEnergy = harvestedEnergy + bsVoltageArray * currentSlot * newSlotTime /1024;
      else
          next_slot_change_time = next_slot_change_time + 24/N_ENERGY_SLOT;
          harvestedEnergy = harvestedEnergy + bsVoltageArray * currentSlot * newSlotTime /1024;
      end
      printf('\n------>New Energy Slot %d, slot %d current %f, endSlot %f\n\n', energySlot, slot, currentSlot, next_slot_change_time);
    end
    debug('Energy harvested\n'); harvestedEnergy
    bsBatteryChargeArray = bsBatteryChargeArray .+ harvestedEnergy;
    batteriesOverLoaded = find((bsBatteryChargeArray.-bsBatteryCapacityArray)>0);
    bsBatteryChargeArray(batteriesOverLoaded) = bsBatteryCapacityArray(batteriesOverLoaded);
    debug('BSs Battery level\n'); %bsBatteryChargeArray

    % update battery consumption
    %  rbsAllocatedPerBsRatio
    energyDrained = bsW0Array .+ (bs_aArray.*rbsAllocatedPerBsRatio)
    energyDrained(sleepBsId) = 0;
    %sleepBsId
    %debug('Energy Drained'); energyDrained

    bsBatteryChargeArray(activeBsId) = bsBatteryChargeArray(activeBsId) .- (energyDrained(activeBsId).*bsBatteryArray(activeBsId))
    bsBatteryChargeArray(find(bsBatteryChargeArray<0)) = 0;
    %bsGridArray
    bsGridEnergyArray = energyDrained(activeBsId).*bsGridArray(activeBsId);

%      debug('BSs Battery power level'); bsBatteryChargeArray
    debug('BS battery level');
    bsBatteryLevel = bsBatteryChargeArray./bsBatteryCapacityArray

    switch (algorithm)
      case 0  % basicUeAllocation
        [rbsActiveBs, usedRbPerBs,ueTrafficAllocated, activeBsId, sleepBsId, ueRealAllocation] = basicUeAllocation(ueTrafficPerSlotArray, slotTrafficProfile(slot), ueAllocation, ueDlSinrMatrix, ueDlSinrArray, rbsAllocatedPerBsRatio, bsBatteryLevel, bsSwitchOffThr, activeBsId, sleepBsId);
      case 3  % qLearningBasic
        [rbsActiveBs, usedRbPerBs,ueTrafficAllocated, activeBsId, sleepBsId, ueRealAllocation, trafficReqPerBs] = qLearningBasic(ueTrafficPerSlotArray, slotTrafficProfile(slot), ueAllocation, ueDlSinrMatrix, ueDlSinrArray, rbsAllocatedPerBsRatio, bsBatteryLevel, bsSwitchOffThr, algo_params,suffixFigure,it, harvestedEnergy, totThroughputNormalized, trafficReqPerBs);
      otherwise
         error('Allocation algorithm not yet implemented');
    endswitch
    rbsAllocatedPerBsRatio(activeBsId) = usedRbPerBs(activeBsId)./totalRbPerBs(activeBsId);
    rbsAllocatedPerBsRatio(sleepBsId) = 0;
    rbsAllocatedPerActiveBsRatio(activeBsId) = usedRbPerBs(activeBsId)./(rbsActiveBs(activeBsId)*1000*60*60*timeSlot);
    rbsAllocatedPerBsRatio(sleepBsId) = 0;
    printf('Macro load %f, sc load %f\n', rbsAllocatedPerBsRatio(1), rbsAllocatedPerBsRatio(2));
    totThroughputNormalized = sum(ueTrafficAllocated)/sum(ueTrafficPerSlotArray*slotTrafficProfile(slot));
    printf('Traffic served %g, traffic req %g -> ratio %g\n', sum(ueTrafficAllocated), sum(ueTrafficPerSlotArray*slotTrafficProfile(slot)), totThroughputNormalized);

    slotId = slot+((day-1)*24);
    slotId = it;
    % save data into files
    fprintf(fpBatteryLevel,'%d',slotId);
    for i=1:N_BS
      fprintf(fpBatteryLevel,' %f',bsBatteryLevel(i));
    endfor
    fprintf(fpBatteryLevel,'\n',bsBatteryLevel(i));

    fprintf(fpEnergyDrained,'%d',slotId);
    for i=1:N_BS
      fprintf(fpEnergyDrained,' %f',energyDrained(i));
    endfor
    fprintf(fpEnergyDrained,'\n',energyDrained(i));

    fprintf(fpRbsActiveBs,'%d',slotId);
    for i=1:N_BS
      fprintf(fpRbsActiveBs,' %f',rbsActiveBs(i));
    endfor
    fprintf(fpRbsActiveBs,'\n',rbsActiveBs(i));

    fprintf(fpUsedRbPerBs,'%d',slotId);
    for i=1:N_BS
      fprintf(fpUsedRbPerBs,' %f',rbsAllocatedPerBsRatio(i));
    endfor
    fprintf(fpUsedRbPerBs,'\n',rbsAllocatedPerBsRatio(i));

%    fprintf(fpUeTrafficAllocated,'%d',slotId);
%      for i=1:N_UE
%        fprintf(fpUeTrafficAllocated,' %f',ueTrafficAllocated(i));
%      endfor
%      fprintf(fpUeTrafficAllocated,'\n',ueTrafficAllocated(i));

    aggTrafficReq = sum(ueTrafficPerSlotArray*slotTrafficProfile(slot));
    aggThr = sum(ueTrafficAllocated);
    aggThrOffloaded = aggThr - sum(ueTrafficAllocated(find(ueRealAllocation==1)));
    printf('Traffic offloaded = %g\n', aggThrOffloaded);
    scsSleeping = numel (sleepBsId);
    droppedUes = numel (find(ueTrafficAllocated==0));
    trafficDropped = sum(ueTrafficPerSlotArray(find(ueTrafficAllocated==0))*slotTrafficProfile(slot))
    averageScsLoad = sum(rbsAllocatedPerBsRatio(N_MC+1:N_BS))/N_SC;
    minScRbs = min(rbsActiveBs(N_MC+1:N_BS));
    maxScRbs = max(rbsActiveBs(N_MC+1:N_BS));
    aggEnergyWasted = sum(energyDrained);
    averageBatteryLevel = sum(bsBatteryLevel(N_MC+1:N_BS))/N_SC;
    maxBatteryLevel = max(bsBatteryLevel(N_MC+1:N_BS));
    minBatteryLevel = min(bsBatteryLevel(N_MC+1:N_BS));
    batteryBelowThr = numel(find(bsBatteryLevel(N_MC+1:N_BS)<bsSecurityThr(N_MC+1:N_BS)))
    
    
    gridEnergyWasted = sum(bsGridEnergyArray);
    averageMcsLoad = sum(rbsAllocatedPerBsRatio(mcIds))/N_MC;
    peakMcsLoad = max (rbsAllocatedPerBsRatio(mcIds));
    ueThr = ueTrafficAllocated./(ueTrafficPerSlotArray*slotTrafficProfile(slot));
    fairness = (sum(ueThr))^2/(N_UE*sum(ueThr.^2))
    %fprintf(fpAggregatedStats,'%d %f %f %d %d %f %d %d %f %f %f %f\n',slotId,aggTrafficReq, aggThr, scsSleeping, droppedUes, averageScRbs, minScRbs, maxScRbs, aggEnergyWasted, averageBatteryLevel, maxBatteryLevel, minBatteryLevel);
    fprintf(fpHourAggregatedStats,'%d',slotId); %1
    fprintf(fpHourAggregatedStats,' %f',aggTrafficReq); %2
    fprintf(fpHourAggregatedStats,' %f',aggThr); %3
    fprintf(fpHourAggregatedStats,' %d',scsSleeping); %4
    fprintf(fpHourAggregatedStats,' %d',droppedUes); %5
    fprintf(fpHourAggregatedStats,' %f',peakMcsLoad); %6
    fprintf(fpHourAggregatedStats,' %f',averageMcsLoad); %7
    fprintf(fpHourAggregatedStats,' %f',averageScsLoad); %8 
    fprintf(fpHourAggregatedStats,' %d',minScRbs); %9 
    fprintf(fpHourAggregatedStats,' %d',maxScRbs); % 10
    fprintf(fpHourAggregatedStats,' %f',aggEnergyWasted); %11
    fprintf(fpHourAggregatedStats,' %f',gridEnergyWasted); % 12
    fprintf(fpHourAggregatedStats,' %f',averageBatteryLevel); %13
    fprintf(fpHourAggregatedStats,' %f',maxBatteryLevel); %14
    fprintf(fpHourAggregatedStats,' %f',minBatteryLevel); %15
    fprintf(fpHourAggregatedStats,' %f',batteryBelowThr); %16
    fprintf(fpHourAggregatedStats,' %f',totThroughputNormalized); %17
    fprintf(fpHourAggregatedStats,' %f',harvestedEnergy(N_MC+1)); %18 
    fprintf(fpHourAggregatedStats,' %f',fairness); %19 
    fprintf(fpHourAggregatedStats,' %f',trafficDropped); %20
    fprintf(fpHourAggregatedStats,' %f',aggThrOffloaded); %21
    fprintf(fpHourAggregatedStats,'\n');
    
    % store stats on policy
    
    if (find (sampleDayPolicy==day))
      policyFilename = sprintf('energyPolicy_%s_panel%s_scBattery%f_bTh%g_algo%d_NBS%d_N_SC%d_N_UE%d_month%d_day%d_hour%d_seed%d.txt', algo_string, panelName, scBatteryCapacity, batteryThreshold, algorithm, N_BS, N_SC,N_UE, month, day, slot, simSeed);
      fpPolicy = fopen(policyFilename,'w');
      activeBsId
      for i=N_MC+1:N_BS
        if (find(activeBsId==i))
          fprintf (fpPolicy, ' %d', 1);
        else
          fprintf (fpPolicy, ' %d', 0);
        end
      end
      fprintf(fpPolicy,'\n');
      fclose(fpPolicy);
    end


    % store daily stats
    dailyAggTrafficReq = dailyAggTrafficReq + aggTrafficReq;
    dailyAggThr = dailyAggThr + aggThr;
    dailyScsSleeping = dailyScsSleeping+ scsSleeping;
    dailyDroppedUes = dailyDroppedUes+ droppedUes;
    dailyAggEnergyWasted = dailyAggEnergyWasted + aggEnergyWasted;
    dailyAverageBatteryLevel = dailyAverageBatteryLevel + averageBatteryLevel;
    dailyGridEnergyWasted = dailyGridEnergyWasted + gridEnergyWasted;
    dailyAverageMcsLoad = dailyAverageMcsLoad + averageMcsLoad;
    dailyPeakMcsLoad = max(cat(2, dailyPeakMcsLoad, peakMcsLoad));
    dailyHoursBatteryBelowThr = dailyHoursBatteryBelowThr + batteryBelowThr;
    if (minBatteryLevel<dailyMinBatteryLevel)
      dailyMinBatteryLevel = minBatteryLevel;
    end
    if (maxBatteryLevel>dailyMaxBatteryLevel)
      dailyMaxBatteryLevel = maxBatteryLevel;
    end
    minIds = find (dailyMinBatteryLevelArray>bsBatteryLevel)
    dailyMinBatteryLevelArray (minIds) = bsBatteryLevel (minIds);
    dailyAverageNormalizedThroughput = dailyAverageNormalizedThroughput + totThroughputNormalized;
    if (find(winterMonths==monthId))
      statsId = 1;
      else
      statsId = 2;
    end
    simAverageNormalizedThroughput(statsId) = simAverageNormalizedThroughput(statsId) + totThroughputNormalized;
    simAverageNormalizedThroughput_2(statsId) = simAverageNormalizedThroughput_2(statsId) + (totThroughputNormalized^2);
    dailyAvgFairness = dailyAvgFairness + fairness;
    simFairness(statsId) = simFairness(statsId) + fairness;
    dailyThrDrop = dailyThrDrop + trafficDropped;
    simThrDrop(statsId) = simThrDrop(statsId) + trafficDropped;
    dailyAggThrOffloaded = dailyAggThrOffloaded + aggThrOffloaded;
    simAggThrOffloaded(statsId) = simAggThrOffloaded(statsId) + aggThrOffloaded;
    
    
    

  endfor % slot
  
  next_slot_change_time = next_slot_change_time - 24;

  dailyAverageBatteryLevel = dailyAverageBatteryLevel/24;
  dailyAverageMcsLoad = dailyAverageMcsLoad/24;
  dailyAverageNormalizedThroughput = dailyAverageNormalizedThroughput/24;
  dailyAvgFairness = dailyAvgFairness/24;
  
  fprintf(fpDayAggregatedStats,'%d',day); %1
  fprintf(fpDayAggregatedStats,' %f',dailyAggTrafficReq); %2
  fprintf(fpDayAggregatedStats,' %f',dailyAggThr); %3
  fprintf(fpDayAggregatedStats,' %f',dailyScsSleeping); % 4
  fprintf(fpDayAggregatedStats,' %f',dailyDroppedUes); % 5
  fprintf(fpDayAggregatedStats,' %f',dailyAggEnergyWasted); % 6
  fprintf(fpDayAggregatedStats,' %f',dailyAverageBatteryLevel); % 7
  fprintf(fpDayAggregatedStats,' %f',dailyHoursBatteryBelowThr); % 8
  fprintf(fpDayAggregatedStats,' %f',dailyGridEnergyWasted); % 9
  fprintf(fpDayAggregatedStats,' %f',dailyAverageMcsLoad); % 10
  fprintf(fpDayAggregatedStats,' %f',dailyPeakMcsLoad); % 11
  fprintf(fpDayAggregatedStats,' %f',dailyMinBatteryLevel); % 12
  fprintf(fpDayAggregatedStats,' %f',dailyMaxBatteryLevel); % 13
  fprintf(fpDayAggregatedStats,' %f',dailyAverageNormalizedThroughput); % 14
  fprintf(fpDayAggregatedStats,' %f',dailyAvgFairness); % 15
  fprintf(fpDayAggregatedStats,' %f',dailyThrDrop); % 16
  fprintf(fpDayAggregatedStats,' %f',dailyAggThrOffloaded); % 17
  for i=N_MC+1:N_BS
    fprintf(fpDayAggregatedStats,' %f',dailyMinBatteryLevelArray(i));
  end
  fprintf(fpDayAggregatedStats,'\n');
  
  
  if (find(winterMonths==monthId))
    statsId = 1;
  else
    statsId = 2;
  end
  
  if (convergence==0)
      simDays = zeros(1,statsSlotNum);
      simAggTrafficReq = zeros(1,statsSlotNum);
      simAggThr = zeros(1,statsSlotNum);
      simAggThrOffloaded = zeros(1,statsSlotNum);
      simAvgThr = zeros(1,statsSlotNum);
      simScsSleeping = zeros(1,statsSlotNum);
      simDroppedUes = zeros(1,statsSlotNum);
      simAggEnergyWasted = zeros(1,statsSlotNum);
      simAverageBatteryLevel = zeros(1,statsSlotNum);
      simGridEnergyWasted = zeros(1,statsSlotNum);
      simAverageMcsLoad = zeros(1,statsSlotNum);
      simDailyPeakMcsLoad = zeros(1,statsSlotNum);
      simHoursBatteryBelowThr = zeros(1,statsSlotNum);
      simAverageMinBatteryLevel = zeros(1,statsSlotNum);
      simAverageMaxBatteryLevel = zeros(1,statsSlotNum);
      simAverageNormalizedThroughput = zeros(1,statsSlotNum);
      simConvergenceTime = ones(1,statsSlotNum)*-1;
      simConvergenceNum = zeros(1,statsSlotNum);
      simFairness = zeros(1,statsSlotNum);
      simThrDrop = zeros(1,statsSlotNum);


      simAvgThr_2 = zeros(1,statsSlotNum);
      simAverageBatteryLevel_2 = zeros(1,statsSlotNum);
      simAverageMcsLoad_2 = zeros(1,statsSlotNum);
      simAverageMinBatteryLevel_2 = zeros(1,statsSlotNum);
      simAverageMaxBatteryLevel_2 = zeros(1,statsSlotNum);
      simAverageNormalizedThroughput_2 = zeros(1,statsSlotNum)
  end
  simDays(statsId) = simDays(statsId) + 1;

  simAggTrafficReq(statsId) = simAggTrafficReq(statsId) + dailyAggTrafficReq;
  simAggThr(statsId) = simAggThr(statsId) + dailyAggThr;
  simAvgThr(statsId) = simAvgThr(statsId) + dailyAggThr;
  simScsSleeping(statsId) = simScsSleeping(statsId) + dailyScsSleeping;
  simDroppedUes(statsId) = simDroppedUes(statsId) + dailyDroppedUes;
  simAggEnergyWasted(statsId) = simAggEnergyWasted(statsId) + dailyAggEnergyWasted;
  simAverageBatteryLevel(statsId) = simAverageBatteryLevel(statsId) + dailyAverageBatteryLevel;
  simGridEnergyWasted(statsId) = simGridEnergyWasted(statsId) + dailyGridEnergyWasted;
  simAverageMcsLoad(statsId) = simAverageMcsLoad(statsId) + dailyAverageMcsLoad;
  simDailyPeakMcsLoad(statsId) = max(cat(2, simDailyPeakMcsLoad(statsId), dailyPeakMcsLoad));
  simHoursBatteryBelowThr(statsId) = simHoursBatteryBelowThr(statsId) + dailyHoursBatteryBelowThr;
  simAverageMinBatteryLevel(statsId) = simAverageMinBatteryLevel(statsId) + dailyMinBatteryLevel;
  simAverageMaxBatteryLevel(statsId) = simAverageMaxBatteryLevel(statsId) + dailyMaxBatteryLevel;

  simAvgThr_2(statsId) = simAvgThr_2(statsId) + dailyAggThr^2;
  simAverageBatteryLevel_2(statsId) = simAverageBatteryLevel_2(statsId) + dailyAverageBatteryLevel^2;
  simAverageMcsLoad_2(statsId) = simAverageMcsLoad_2(statsId) + dailyAverageMcsLoad^2;
  simAverageMinBatteryLevel_2(statsId) = simAverageMinBatteryLevel_2(statsId) + dailyMinBatteryLevel^2;
  simAverageMaxBatteryLevel_2(statsId) = simAverageMaxBatteryLevel_2(statsId) + dailyMaxBatteryLevel^2;


  if (convergence==0)
    if ((dailyHoursBatteryBelowThr/(24*N_SC))<convergenceThr)
      convergenceSeries = convergenceSeries + 1;
      if (convergenceSeries==convergenceWindow)
        for i=1:statsSlotNum 
          simConvergenceTime(i) = day; % save the day in all stats files
        endfor
        convergence = 1;
        printf('CONVERGED');
        convergenceDay = day;
      end
    else
      convergenceSeries = 0;
    end
  else
    if ((dailyHoursBatteryBelowThr/(24*N_SC))<convergenceThr)
      simConvergenceNum(statsId) = simConvergenceNum(statsId) + 1;
    end
  end
      
  if ((convergenceDay+365==day)&&(convergence==1))
    % enough stats -> break
    printf('End simulation for reaching 365 days of convergence in day %d\n', day);
    break;
  endif
  


  
endfor % day

for i=1:statsSlotNum
  printf('Days per slot %d\n', simDays(i));
  if (simDays(i)>0)
    simAvgThr(i) = simAvgThr(i)/simDays(i);
    simAverageBatteryLevel(i) = simAverageBatteryLevel(i)/simDays(i);
    simAverageMcsLoad(i) = simAverageMcsLoad(i)/simDays(i);
    simAverageMinBatteryLevel(i) = simAverageMinBatteryLevel(i)/simDays(i);
    simAverageMaxBatteryLevel(i) = simAverageMaxBatteryLevel(i)/simDays(i);
    simAverageNormalizedThroughput(i) = simAverageNormalizedThroughput(i)/(simDays(i)*slotsPerDay);
    simFairness(i) = simFairness(i)/(simDays(i)*slotsPerDay);

    % variancies
    simAvgThr_var(i) = (simAvgThr_2(i)/simDays(i)) - (simAvgThr(i)^2);
    simAverageBatteryLevel_var(i) = (simAverageBatteryLevel_2(i)/simDays(i)) - (simAverageBatteryLevel(i)^2);
    simAverageMcsLoad_var(i) = (simAverageMcsLoad_2(i)/simDays(i)) - (simAverageMcsLoad(i)^2);
    simAverageMinBatteryLevel_var(i) = (simAverageMinBatteryLevel_2(i)/simDays(i)) - (simAverageMinBatteryLevel(i)^2);
    simAverageMaxBatteryLevel_var(i) = (simAverageMaxBatteryLevel_2(i)/simDays(i)) - (simAverageMaxBatteryLevel(i)^2);
    simAverageNormalizedThroughput_var(i) = (simAverageNormalizedThroughput_2(i)/simDays(i)) - (simAverageNormalizedThroughput(i)^2);

    % convergence stats
    simConvergenceRatio(i) = simConvergenceNum(i) / simDays(i)
    
    if (i==1)
      simAggregatedStatsFilename = sprintf('simAggregatedStatsWinter_%s', suffixSim);
    else
      simAggregatedStatsFilename = sprintf('simAggregatedStatsSummer_%s', suffixSim);
    end
    fpSimAggregatedStats = fopen(simAggregatedStatsFilename,'a');
    fprintf(fpSimAggregatedStats,'%d',simSeed); %1
    fprintf(fpSimAggregatedStats,' %f',simAggTrafficReq(i)); %2
    fprintf(fpSimAggregatedStats,' %f',simAggThr(i)); %3
    fprintf(fpSimAggregatedStats,' %f',simScsSleeping(i)); %4
    fprintf(fpSimAggregatedStats,' %f',simDroppedUes(i)); %5
    fprintf(fpSimAggregatedStats,' %f',simAggEnergyWasted(i)); %6
    fprintf(fpSimAggregatedStats,' %f',simAverageBatteryLevel(i)); %7
    fprintf(fpSimAggregatedStats,' %f',simHoursBatteryBelowThr(i)); %8
    fprintf(fpSimAggregatedStats,' %f',simGridEnergyWasted(i)); %9
    fprintf(fpSimAggregatedStats,' %f',simAverageMcsLoad(i)); %10
    fprintf(fpSimAggregatedStats,' %f',simDailyPeakMcsLoad(i)); %11
    fprintf(fpSimAggregatedStats,' %f',simAverageMinBatteryLevel(i)); %12
    fprintf(fpSimAggregatedStats,' %f',simAverageMaxBatteryLevel(i)); %13
    fprintf(fpSimAggregatedStats,' %f',simConvergenceTime(i)); %14
    fprintf(fpSimAggregatedStats,' %f',simConvergenceRatio(i)); %15
    fprintf(fpSimAggregatedStats,' %f',simAverageNormalizedThroughput(i)); %16
    fprintf(fpSimAggregatedStats,' %f',simAverageNormalizedThroughput_var(i)); %17
    fprintf(fpSimAggregatedStats,' %f',simFairness(i)); %18
    fprintf(fpSimAggregatedStats,' %f',simThrDrop(i)); %19
    fprintf(fpSimAggregatedStats,' %f',simAggThrOffloaded(i)); %20
    fprintf(fpSimAggregatedStats,'\n');
    fclose(fpSimAggregatedStats);
  end
end

fclose(fpBatteryLevel);
fclose(fpEnergyDrained);
fclose(fpRbsActiveBs);
fclose(fpUsedRbPerBs);
fclose(fpUeTrafficAllocated);
fclose(fpHourAggregatedStats);
fclose(fpDayAggregatedStats);




endfunction
