%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%


function [rbsActiveBs, usedRbPerBs,ueTrafficAllocated, activeBsId, sleepBsId, ueAllocation, trafficReqPerBs] = qLearningBasic (ueTrafficPerSlotArray, slotTrafficCoeff, ueAllocation, ueDlSinrMatrix, ueDlSinrArray, rbsAllocatedPerBsRatio, bsBatteryLevel, bsSwitchOffThr, algo_params, suffixFigure, it, harvestedEnergy, totThroughputNormalized, trafficReqPerBs)
global debuglevel;
global N_UE;
global N_BS;
global N_MC;
global N_RB;
global timeSlot;

%  algo_params = { X, % alpha learning rate
%                  X, % gamma, discount factor
%                  X, % epsilon, greedy factor
%                  X, % battery levels (state)
%                  X, % load levels (action)
%  };

persistent alpha = algo_params{1};
persistent discount_factor = algo_params{2};
persistent epsilon = algo_params{3};
persistent macroLoadThr = algo_params{4};
persistent batteryQlevels = algo_params{5};
persistent loadQlevels = algo_params{6};
persistent batteryThr = algo_params{7};
persistent loadDeltaThr = algo_params{8};
persistent Kvalue = algo_params{9};
persistent negativeReward = algo_params{10};
persistent solarSlotNum = algo_params{11};
persistent epsilonThr = algo_params{12};


persistent actionLevels = 0;
persistent macroLoadLevelsNum = 0;

% initialize internal parameters
if (it==1)
  alpha = algo_params{1};
  discount_factor = algo_params{2};
  epsilon = algo_params{3};
  macroLoadThr = algo_params{4};
  batteryQlevels = algo_params{5};
  loadQlevels = algo_params{6};
  batteryThr = algo_params{7};
  loadDeltaThr = algo_params{8};
  Kvalue = algo_params{9};
  negativeReward = algo_params{10};
  solarSlotNum = algo_params{11};
  epsilonThr = algo_params{12};
  actionLevels = 0;
  macroLoadLevelsNum = 0;
end
  

% initialize action levels for having state 0 with level 0 at 1st iteration
if (it==1)
  for i=1:loadQlevels-1
    actionLevels = cat(1, actionLevels, i/(loadQlevels-1));
  endfor
  actionLevels
end

if (it==1)
  if (macroLoadThr>1)
    % loadThrLevels
    macroLoadLevels = 0;
    for i=1:macroLoadThr-1
      macroLoadLevels = cat(1, macroLoadLevels, i/(macroLoadThr));
    endfor
    macroLoadLevels
    macroLoadLevelsNum = macroLoadThr;
  else
    macroLoadLevelsNum = 2;
  end
end

macroLoadLevelsNum


persistent bsStateArray = {};
persistent bsQTableArray = {};
persistent actionArray = zeros(1,N_BS);
persistent rbsActiveBs = ones(1,N_BS) * N_RB;
persistent activeBsId = 1:N_BS;

%persistent trafficReqPerBs = zeros(1,N_BS);
persistent potentialLoadPerBs = zeros(1,N_BS);

scStartId = 1+N_MC;

totalRbPerBs = ones(1,N_BS)*N_RB*1000*60*60*timeSlot;
availableRbPerBs = rbsActiveBs*1000*60*60*timeSlot;

% convert battery at 0 to non-zero values (for using ceil())
zerosId = find(bsBatteryLevel==0);
bsBatteryLevel(zerosId) = 0.001; % fake value for avoiding errors with ceil()

debug = 1;
qtableLog = 0;
qtableStep = 2000;

% constraint load to 1.0
potentialLoadPerBs = trafficReqPerBs./totalRbPerBs;
potentialLoadPerBs(find(potentialLoadPerBs>1)) = 1;

if (it==1)
  % initialize the state and qTable matrixes of Q-Learning
  if (N_MC~=1)
     error('Only one macro supported');
  endif
  macroLoad = macroLoadLevelsNum;
  for i=1:N_BS
     bsStateArray{i} = {1,ceil(bsBatteryLevel(i)*batteryQlevels), macroLoad}
     bsQTableArray{i} = zeros (solarSlotNum,batteryQlevels, macroLoadLevelsNum, loadQlevels)
  endfor
else
  % update states and qvalues
%    if (rbsAllocatedPerBsRatio(1)>macroLoadThr)
%      macroLoad = 2;
%    else
%      macroLoad = 1;
%    endif
  if (macroLoadThr<1)
    if (potentialLoadPerBs(1)>macroLoadThr)
      macroLoad = 2;
    else
      macroLoad = 1;
    endif
  else
    if (potentialLoadPerBs(1)==0)
      macroLoad = 1
    else
      macroLoad = ceil(potentialLoadPerBs(1)*macroLoadThr);
    end
  endif
  for i=scStartId:N_BS
     if (macroLoadThr<1)
        if (potentialLoadPerBs(i)>macroLoadThr)
          macroLoad = 2;
        else
          macroLoad = 1;
        endif
      else
        if (potentialLoadPerBs(i)==0)
          macroLoad = 1
        else
          macroLoad = ceil(potentialLoadPerBs(i)*macroLoadThr);
          if (macroLoad==0)
            macroLoad = 1;
          end
          if (macroLoad>macroLoadThr)
            macroLoad = macroLoadThr;
          end
        end
    endif
    if (harvestedEnergy(i)<1e-3)
      solarState = 1; % night
    else
      solarState = 2; % day
    end
     if (i==2)&&(debug==1)
       printf('RUN BASIC Q-LEARNING, SC %d, it %d , Load %f->%d\n', i, it, potentialLoadPerBs(i), macroLoad);
     endif
     % Evaluate the reward of the previous iteration
     if (actionArray(i)~=0)
        newBsStateArray{i} = {solarState, ceil(bsBatteryLevel(i)*batteryQlevels), macroLoad};

        % battery and cell throughput 
        minThr = 0.97; % max 3% of dropped traffic allowed
        if (rbsAllocatedPerBsRatio(i)>0)
           u = 1;
        else
           u = 0;
        end
        if ((bsBatteryLevel(i)<batteryThr)||(totThroughputNormalized<minThr))
           reward = negativeReward;
        else
          reward = ((1-u)*(1/bsBatteryLevel(i))) + (10 * u * rbsAllocatedPerBsRatio(i));
        endif
        %printf('Traffic %g, battery %g -> reward=%g\n', rbsAllocatedPerBsRatio(i), bsBatteryLevel(i), reward);




        bsState = bsStateArray{i};
        bsStateBattery = bsState{2}
%          if (i==2)
%            printf('Battery %d\n', bsStateBattery);
%          endif
        bsStateLoad = bsState{3}
        bsStateSolar = bsState{1}
        bsQTable = bsQTableArray{i};
        newbsState = newBsStateArray{i};
        newbsStateBattery = newbsState{2};
        newbsStateLoad = newbsState{3};
        newbsStateSolar = newbsState{1};
        newbsQTable = bsQTableArray{i};
        maxQvalue = max(bsQTable(bsStateSolar,bsStateBattery, bsStateLoad,:));
        if (i==2)&&(debug==1)
          printf('Update (%d,%d,%d,%d), reward %d, energy %f sc-load %f solar %d\n', bsStateSolar,bsStateBattery, bsStateLoad,actionArray(i), reward, bsBatteryLevel(i), potentialLoadPerBs(i), bsStateSolar);
          printf('SC %d New Qvalue (%d,%d,%d,%d) = %f + %f (%f + %fx%f - %f)\n', i, bsStateSolar,bsStateBattery, bsStateLoad, actionArray(i),bsQTable(bsStateSolar, bsStateBattery, bsStateLoad, actionArray(i)),alpha, reward, discount_factor,maxQvalue,bsQTable(bsStateSolar,bsStateBattery, bsStateLoad, actionArray(i)));
        endif
        % Update Q table 
        bsQTable(bsStateSolar, bsStateBattery, bsStateLoad, actionArray(i)) = bsQTable(bsStateSolar, bsStateBattery, bsStateLoad,actionArray(i)) + alpha * (reward + (discount_factor*maxQvalue) - bsQTable(bsStateSolar, bsStateBattery, bsStateLoad, actionArray(i)));
        if (i==2)&&(debug==1)
          printf('New q-value %f\n', bsQTable(bsStateSolar,bsStateBattery, bsStateLoad,actionArray(i)));
        endif
        bsQTableArray{i} = bsQTable;
%          if (i==2)
%            bsQTable
%          endif
        bsStateArray{i} = newBsStateArray{i};
     endif
     % save qtables
     if ((mod(it+1,qtableStep)==0)&&(qtableLog==1))
        printf('QTABLE SAVING\n');
        for s=1:solarSlotNum
          printf('STATE %d\n', s);
          bsQTable = bsQTableArray{i};
          bsQTableSlot = bsQTable(s,:, :,:)
          [qmaxtable indextable]= max(bsQTableSlot,[],4);
          indextable
          %qmaxtable
          filename = sprintf('maxqtable_%s_state%d_it%d.txt', suffixFigure, s, it+1);
          fpq = fopen(filename,'w');
          filename = sprintf('policy_%s_state%d_it%d.txt', suffixFigure, s, it+1);
          fpp = fopen(filename,'w');
          for j=1:batteryQlevels
            fprintf(fpq,'%d',j);
            fprintf(fpp,'%d',j);
            for k=1:macroLoadLevelsNum
              %printf('j=%d, k=%d\n', j,k);
              fprintf(fpq,' %g',qmaxtable(1,j,k));
              fprintf(fpp,' %d',indextable(1,j,k));
            end
            fprintf(fpq,'\n');
            fprintf(fpp,'\n');
          end
          fclose(fpq);
          fclose(fpp);
          xaxis=1:batteryQlevels;
          % Max QTable Figure
          f = figure(1);
          ptsymb = {'-b','-r','-g','-m','-c','-r+','-b*','-cx','-ms','-yd','kv','b^','rs','m+','gs','c^','rv','b+','m^','g^','co','rd','mo','g+'};


          for j=1:macroLoadLevelsNum
            plot(xaxis, qmaxtable(1,:,j),ptsymb{j});
            %qmaxtable(:,j)
            legendstring{1,j} = sprintf('Load %d', j);
            hold on;
          end
          hold off;
          legend (legendstring, 'location', 'east');
          legend boxon
          legend hide
          legend show
          titletag = sprintf('Max Q table for state %d after %d iterations', s, it+1);
          %title('Max Q table');
          title(titletag);
          xlabel ('Energy levels');
          ylabel ('Max Q values');
          filename = sprintf('maxqtable_%s_state%d_it%d.eps', suffixFigure, s, it+1);
          print(1,filename, '-color','-deps');

          % Policy Figure
          f = figure(2);
          ptsymb = {'-b','-r','-g','-m','-c','-r+','-b*','-cx','-ms','-yd','kv','b^','rs','m+','gs','c^','rv','b+','m^','g^','co','rd','mo','g+'};
          for j=1:macroLoadLevelsNum
            plot(xaxis, indextable(1,:,j),ptsymb{j});
            %indextable(:,j)
            legendstring{1,j} = sprintf('Load %d', j);
            hold on;
          end
          hold off;
          legend (legendstring, 'location', 'east');
          legend boxon
          legend hide
          legend show
          titletag = sprintf('Policy for state %d after %d iterations', s, it+1);
          %title('Max Q table');
          title(titletag);
          xlabel ('Energy levels');
          ylabel ('Action');
          filename = sprintf('policy_%s_state%d_it%d.eps', suffixFigure, s, it+1);
          print(2,filename, '-color','-deps');
        end %for qtables
     endif

  endfor

endif

% q-learning algorithm
for i=scStartId:N_BS
  if ((rand(1)>epsilon)||(it>epsilonThr))
    % run q-learning
%      printf('Run Q-learning process for SC %d\n', i);
    bsState = bsStateArray{i};
    bsStateBattery = bsState{2};
    bsStateLoad = bsState{3};
    bsStateSolar = bsState{1};
    bsQTable = bsQTableArray{i};
    if (i==2)&&(debug==1)
      bsBatteryLevel(i)
      bsBatteryLevel(i)*batteryQlevels
      ceil(bsBatteryLevel(i)*batteryQlevels)
      bsStateBattery
      bsStateLoad
      bsStateSolar
      bsQTable(bsStateSolar,bsStateBattery, bsStateLoad,:)
    endif
    % ASSUMPTION take the maximum action value available
    bsQTable
    bsQTable(bsStateSolar,bsStateBattery, bsStateLoad,:)
    % Evaluate action according to Q-Learning algorithm
    action = max(find (bsQTable(bsStateSolar,bsStateBattery, bsStateLoad,:)==max (bsQTable(bsStateSolar,bsStateBattery, bsStateLoad,:))));
    if (debug==1)
       printf('Max Q value for SC %d action %d\n', i, action);
     end
   else
     action = ceil(rand(1)*loadQlevels);
     if (debug==1)
       printf('Run greedy process for SC %d action %d it %d\n', i, action, it);
     end
%       printf('Run greedy process for SC %d action %d\n', i, action);
   end
   % update BS according action
   actionArray(i) = action;
   rbsActiveBs(i) = round (N_RB*actionLevels(action));
   if (i==2)&&(debug==1)
       printf('RBs Profile\n');
       rbsActiveBs
   endif
endfor





% udpate bs status
% reactivate all BSs
%rbsActiveBs = ones(1,N_BS) * N_RB;
% Switch off BSs under threshold
bsOutOfBatteriesIds = find ((bsBatteryLevel.-bsSwitchOffThr)<0);
rbsActiveBs(bsOutOfBatteriesIds) = 0;
activeBsId = find(rbsActiveBs>0);
%debug ('Sleep BSs:');
sleepBsId = find(rbsActiveBs==0);


% first re-allocate UE disconnected
for ue=1:N_UE
    if (ueAllocation(ue)==0)
      ueAllocationTemp = max (ueDlSinrMatrix');
      ueDlSinrArray = ueAllocation;
      ueAllocation(i) = find (ueDlSinrMatrix(i,:)==ueAllocation(i));
    end
endfor


slotTrafficArray = ueTrafficPerSlotArray * slotTrafficCoeff;
ueTbSizeArray = get_tb_size (ueDlSinrArray);
ueRbReq = (slotTrafficArray*1024*1024)./ueTbSizeArray;
%  if (debug==1)
%    printf('Ue RBs Req');
%    ueRbReq
%    printf('Tot RBs %f\n', sum(ueRbReq));
%  end

usedRbPerBs = zeros(1,N_BS);
ueTrafficAllocated = zeros(1,N_UE);

% evaluate total requested traffic per BS
trafficReqPerBs = zeros(1,N_BS);
potentialLoadPerBs = zeros(1,N_BS);
for i=1:N_BS
  trafficReqPerBs(i) = sum(ueRbReq(find(ueAllocation()==i)));
  potentialLoadPerBs(i) = trafficReqPerBs(i)/totalRbPerBs(i);
  if (debug==1)
    printf('BS %d total traffic requested %f which correspond to a load of %f\n', i, trafficReqPerBs(i), potentialLoadPerBs(i));
  end

end

for ue=1:N_UE
   if (ueRbReq>0)
      if (availableRbPerBs(ueAllocation(ue))>=ueRbReq(ue)+usedRbPerBs(ueAllocation(ue)))
          % UE fit in assignted BS
          usedRbPerBs(ueAllocation(ue)) += ueRbReq(ue);
          ueTrafficAllocated(ue) = slotTrafficArray(ue);
%            printf('UE %d(%f) allocated to BS %d(%f)\n', ue, ueRbReq(ue), ueAllocation(ue), usedRbPerBs(ueAllocation(ue)));
      else
          % check for another BS
          dlSinrArray = ueDlSinrMatrix(ue,:);
          bsNum = 1;
          dlSinrArray(ueAllocation(ue)) = -Inf;
          while (bsNum<N_BS)
            max_sinr = max(dlSinrArray);
            ueAllocation(ue) = find (dlSinrArray==max_sinr);
            tbSize = get_tb_size (max_sinr);
            if (tbSize>0)
              ueRbReq(ue) = (slotTrafficArray(ue)*1024*1024)/tbSize;
              if (availableRbPerBs(ueAllocation(ue))>=ueRbReq(ue)+usedRbPerBs(ueAllocation(ue)))
                usedRbPerBs(ueAllocation(ue)) += ueRbReq(ue);
                ueTrafficAllocated(ue) = slotTrafficArray(ue);
                ueDlSinrArray(ue) = max_sinr;
                break;
              else
                dlSinrArray(ueAllocation(ue)) = -Inf;
              end
            else
              % UE out fo coverage of any BS
              ueAllocation(ue) = 0; % user dropped
              break;
            endif
            bsNum = bsNum + 1;
          endwhile
          if ((ueAllocation(ue)~=0)&&(dlSinrArray(ueAllocation(ue)) == -Inf))
              ueAllocation(ue) = 0; % user dropped
          endif
      endif
   else
     % UE out fo coverage of any BS
     ueAllocation(ue) = 0; % user dropped
  endif
endfor
%debug ('UE Allocation'); ueAllocation
endfunction