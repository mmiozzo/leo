%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

function g = loss_OH_large_cities_urban(d, hb, hm, f)

  %%
  %% function g = loss_OH_large_cities_urban(d, hb, hm, f)
  %%
  %% returns the loss at d meters for f frequency and mobile height m and
  %% base station height of hb
  
  assert(isscalar(f));
  assert(f > 0);

  if (f<200)
    Ch = 8.29*(log10(1.54*hm)).^2-1.1;
  else
    Ch = 3.2*(log10(11.75*hm)).^2-4.97;
  endif
  Ch

  g = [];
  for i = 1:numel(d(:,1))
    gi = zeros(size(d(i,:)));
    di = d(i,:);
    gi(find(di > 0)) = 69.55 + (26.16*log10(f)) - (13.82*log10(hb)) + (44.9-(6.55*log10(hb))).*log10(di*1e-3) - Ch(i);

    gi(find(d <= 0)) = 1;
    g = cat(1, g, gi);
  end
  
       
  