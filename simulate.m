%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

% script for generating simulation campaign

close all;
clear all;

N_RUN = 5;
INIT_SEED = 1;
%nDays = 180;
nDays = 100;
scNum = 4;
mcNum = 1;
uePerSc = 120;
ueNum = uePerSc*scNum;
% uePerSc
% when 0 uniform distribution in the whole area
% when x~=0 uniform distribution of x UEs in each SC and then in macro if remain
rbNum = 25;
debugLevel = 1;
%algorithm = 0; % basicUeAllocation
%algorithm = 3; % qLearningBasic
algorithm = 3; % qLearningBasicSC
batteryThreshold = [0.1 0.3];

switch (algorithm)
case {0,1,3,4,5,6}
  algo_params = { 0.5, % 1 alpha learning rate
                  0.9, % 2 gamma, discount factor
                  0.1, % 3 epsilon, greedy factor
                  3, % 4 macro load threshold
                  5, % 5 battery levels
                  2, % 6 action load levels
                  0.3, % 7 reward battery threshold
                  0.1, % 8 reward load distance threshold
                  1000, % 9 positive reward K load coefficient (i.e., battery above thr)
                  0, % 10 cost or negative reward (i.e., battery below thr)
                  2, % 11 number of solar slots
                  2000000000000, % 12 thershold in number of iteration to stop the epsilon greedy
   };
otherwise
  error('Allocation algorithm not yet implemented');
end

algo_string = sprintf('K%d_C%d_abth%g', algo_params{9}, algo_params{10}, algo_params{7});

cdf_matrixes = {
  'cdf_matrixes_LA_panel1_6x6_vref12V_6sph_12clusters.mat'
  'cdf_matrixes_LA_panel1_16x16_vref24V_6sph_12clusters.mat'
};

panelName = {
  '6x6_12V_12cl' % 78x78 cm ~ 0.608 m2
  '16x16_24V_12cl'  % 260x260 cm ~ 6.76 m2
};

month = 0; % when in [1..12] run the simulation on a single month, otherwise move across months

%  scVoltage = 12;
    % Test programs (to be commented when running simulation campaign)
    % PICO
    %main (1, scNum, 12, mcNum, 0.2, ueNum, uePerSc, rbNum, algorithm, debugLevel, algo_params, cdf_matrixes{2}, panelName{2}, 12, nDays, algo_string, month, batteryThreshold(1))

    % MICRO
    %main (2, scNum, 24, mcNum, 2, ueNum, uePerSc, rbNum, 1, debugLevel, algo_params, cdf_matrixes{3}, panelName{3}, 12, nDays, algo_string, month, batteryThreshold(1))
    %main (2, scNum, 24, mcNum, 2, ueNum, uePerSc, rbNum, 1, debugLevel, algo_params, cdf_matrixes{4}, panelName{4}, 12, nDays, algo_string, month, batteryThreshold(1))
    main (3, scNum, 24, mcNum, 2, ueNum, uePerSc, rbNum, algorithm, debugLevel, algo_params, cdf_matrixes{2}, panelName{2}, 12, nDays, algo_string, month, batteryThreshold(1))
    exit(1);
     
for simSeed=INIT_SEED:N_RUN
  for algo=3:3:3
    for panelType=1:numel(cdf_matrixes)
      % adjust variables
      if (panelType==1)
        scVoltage = 12; % V
        scBatteryCapacity = 0.2; %[KWh]
      else
        scVoltage = 24; % V
        scBatteryCapacity = 2; %[KWh]
      end
  %      if (mod(panelType,2)==0)
  %        nClusters = 6;
  %      else
          nClusters = 12;
  %      end
      scVoltage
      nClusters
      cdf_matrixes{panelType}

      for bth=1:numel(batteryThreshold)
        algo_params{7}=batteryThreshold(bth);
        if (algo==3)
          phyBatteryThr = 0.1;
        else
          phyBatteryThr = batteryThreshold(bth);
        end
        algo_string = sprintf('K%d_C%d_abth%g', algo_params{9}, algo_params{10}, algo_params{7});
        % run simulation
        main (simSeed, scNum, scVoltage, mcNum, scBatteryCapacity, ueNum, uePerSc, rbNum, algo, debugLevel, algo_params, cdf_matrixes{panelType}, panelName{panelType}, nClusters, nDays, algo_string, month, phyBatteryThr);
      end % Bth

    end % panelType

  end % algo_params
end % seed
