%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%


function s = get_tb_size(snr)

  %%
  %% function g = get_tb_size(snr)
  %%
  %% returns the TB size chosen by the LTE AMC for a single physical
  %% resource block at the given snr in dB (which can be a matrix)
  %%

  s = ones(size(snr)) * -1;

  %% SNR threshold identifying the smallest SNR for each TB size
  persistent thresholds = [-7.00 2
			   -4.40 4
			   -2.20 7
			   -0.60 11
			   1.60 20
			   4.00 17
			   5.20 22
			   8.60 28
			   8.80 35
			   10.20 41
			   12.60 47
			   14.60 55
			   16.20 65
			   18.00 73			
			   19.00 89];

  for ii = 1:size(thresholds,1)
    s(find(snr >= thresholds(ii,1))) = thresholds(ii,2);
  endfor
  
endfunction	   
	   