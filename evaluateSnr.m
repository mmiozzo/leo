%  
%  LEO: Lte with Energy-harvesting Octave simulator
%  
%  Copyright (C) 2016  Marco Miozzo
%                      Email: mmiozzo@cttc.es
%                      CTTC: Centre Tecnològic de Telecomunicacions de Catalunya, Barcelona, Spain
%  
%  This program is free software: you can redistribute it and/or modify
%  it under the terms of the GNU General Public License as published by
%  the Free Software Foundation, either version 3 of the License, or
%  (at your option) any later version.
%  
%  This program is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%  GNU General Public License for more details.
%  
%  You should have received a copy of the GNU General Public License
%  along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

function sinrMatrix = evaluateSnr(powerArray, lossMatrix, noisePower)

  %%
  %% function sinr = evaluateSinr(powerArray, lossMatrix, noisePower)
  %%
  %% returns the loss at d meters for f frequency and mobile height m and
  %% base station height of hb

  assert(numel(powerArray)>1); % at least one interference
  powerMatrix = [];
  for i = 1:numel(lossMatrix(:,1))
    powerMatrix = cat (1, powerMatrix, powerArray);
  end
  %powerMatrix
  printf('Matrix of received power per UE (row) per BS (col)\n');
  powerMatrix = powerMatrix - lossMatrix
  sinrMatrix = [];

  for i = 1:numel(lossMatrix(:,1))
    sinr_i = [];
    for j = 1:numel(lossMatrix(1,:))
      %fprintf ('process %d,%d -> %f\n', i, j, lossMatrix(i,j));
      %powerMatrix(i,:)
      i_id = 1:numel(lossMatrix(1,:));
      i_id(:,j) = [];
      %powerMatrix(i,i_id)
      %x = powerMatrix(i,i_id)
      %xdb = 10*log10(sum (10.^(powerMatrix(i_id)/10)) + 10^(noisePower/10))
      %noisePower
      %sinr_ij = powerMatrix(i,j) - 10*log10(sum (10.^(powerMatrix(i,i_id)/10))) - noisePower;
      sinr_ij = powerMatrix(i,j) - noisePower;  % SINR
      sinr_i = cat (2, sinr_i, sinr_ij);
    end
    sinrMatrix = cat(1, sinrMatrix, sinr_i);
  end
%sinrMatrix
